⚠️ This project has been archived. Use [LibRedirect](https://github.com/libredirect/libredirect) instead!
- [Firefox](https://addons.mozilla.org/firefox/addon/libredirect/)
- [Chromium-based browsers (Brave, Google Chrome)](https://github.com/libredirect/libredirect#install-in-chromium-brave-and-chrome)
- [Edge](https://microsoftedge.microsoft.com/addons/detail/libredirect/aodffkeankebfonljgbcfbbaljopcpdb)

# Librarian-Redirects

Redirect Odysee links to [Librarian](https://codeberg.org/imabritishcow/librarian)!

## Chrome(ium)
1. Go to `chrome://extensions` and enable "Developer mode"
2. Download the latest .crx from the Releases tab
3. Drag the downloaded .crx file into `chrome://extensions`